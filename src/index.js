import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import userReducer from './redux/reducers/userReducer';
import loadingReducer from './redux/reducers/loadingReducer';
import { configureStore } from '@reduxjs/toolkit';
// react slick
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export const store = configureStore({
  reducer: {
    userReducer: userReducer,
    loadingReducer: loadingReducer,
  }
});
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
<Provider store={store}>
  <App />
</Provider>

);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
