import React from 'react'
import { NavLink } from 'react-router-dom';
import { Card } from 'antd'; 
import Slider from 'react-slick';
import '../../../../assets/movie-project.css'
const { Meta } = Card;


export default function MovieListMobile({movieArr}) {
    console.log('MovieList - movieArr: ', movieArr)
    const renderMovieList = () => {
        return movieArr.slice(0,50).map((item) => {
            return (    
                <Card
                    className='mt-2'
                    key={item.maPhim}
                    hoverable   
                    cover={<img className='w-50 h-80 object-cover rounded-lg p-5 overflow-hidden' alt="example" src={item.hinhAnh} />}
                >
                    <Meta title={item.tenPhim} style={{height: '100px'}}
                    description={item.moTa.length < 100 ? item.moTa : item.moTa.slice(0, 100) + ' ... '}
                    />
                    <NavLink to={`/detail/${item.maPhim}`} className={'bg-indigo-500 my-5 px-5 py-2 rounded text-white shadow hover:shadow-xl transition duration-500 hover:bg-indigo-700 hover:text-white'}>
                        Xem chi tiết
                    </NavLink>
                </Card>
                
            )
        })
    }

    // react slick
    const settings = {
        dots: true,
        arrows: false,
        draggable: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        rows: 5,
        // centerMode: true,
        // centerPadding: '60px',
      };

  return (
    <> 
        <div style={{padding:'50px 0'}}>
            <Slider {...settings} >
                {renderMovieList()}
            </Slider>
        </div>
    </>
  )
}
