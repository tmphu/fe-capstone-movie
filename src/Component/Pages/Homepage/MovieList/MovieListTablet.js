import React from 'react'
import { NavLink } from 'react-router-dom';
import { Card } from 'antd'; 
import Slider from 'react-slick';
import '../../../../assets/movie-project.css'
const { Meta } = Card;


export default function MovieListTablet({movieArr}) {
    console.log('MovieList - movieArr: ', movieArr)
    const renderMovieList = () => {
        return movieArr.slice(0,50).map((item) => {
            return (
                <Card className='mb-2'
                    key={item.maPhim}
                    hoverable
                    style={{
                    width: '100%',
                    }}
                    cover={<img className='w-50 h-80 object-cover' alt="example" src={item.hinhAnh} />}
                >
                    <Meta title={item.tenPhim} style={{height: '100px'}}
                    description={item.moTa.length < 50 ? item.moTa : item.moTa.slice(0, 50) + ' ... '}
                    />
                    <NavLink to={`/detail/${item.maPhim}`} className={'bg-indigo-500 my-5 px-5 py-2 rounded text-white shadow hover:shadow-xl transition duration-500 hover:bg-indigo-700 hover:text-white'}>
                        Xem chi tiết
                    </NavLink>
                </Card>
            )
        })
    }

    // react slick
    const settings = {
        dots: true,
        arrows: false,
        draggable: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
        rows: 2,
        // centerMode: true,
        // centerPadding: '60px',
      };

  return (
    <> 
        <div style={{padding:'50px 0'}}>
            <Slider {...settings} >
                {renderMovieList()}
            </Slider>
        </div>
    </>
  )
}
