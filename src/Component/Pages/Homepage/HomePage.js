import React, { useEffect, useState } from 'react'
import { movieService } from '../../../services/movieService';
import Footer from '../Footer/Footer';
import HomeCarousel from './HomeCarousel/HomeCarousel';
import MovieList from './MovieList/MovieList';
import MovieTab from './MovieTab/MovieTab';

export default function HomePage() {
    // set state for carousel
    const [bannerArr, setBannerArr] = useState([]);
    useEffect(() => {
        movieService.getMovieBanner()
        .then((res) => {
        // console.log('HomeCarousel - setBannerArr: ', res);
        setBannerArr(res.data.content)

        })
        .catch((err) => {
        console.log(err);
        });
    },[]);
    // set state for Movie array
    const [movieArr, setMovieArr] = useState([]);
    // useEffect will automatically active when component called
    useEffect(() => {
        // stage 1 of useEffect: didMount (born)
        movieService.getMovieList()
        .then((res) => {
            console.log('HomePage - getMovieList: ', res);
            setMovieArr(res.data.content)
        })
        .catch((err) => {
            console.log(err);
        });
        return () => {
            // stage 2 of useEffect: willUnmount (dead)
        }
    },[
        // stage 3 of useEffect: didUpdate (there is updatable state here if needed. If any state we need to run once, keep this [] blank)
    ])

    
    
  return (
    <div>
        <div>
            <HomeCarousel bannerArr={bannerArr}></HomeCarousel>
        </div>
        <div className='container mx-auto'>
            <MovieList movieArr={movieArr}></MovieList>
            <MovieTab></MovieTab>
        </div>
        <div className='pt-5'>
            <hr />
            <Footer></Footer>
        </div>
    </div>
  )
}
