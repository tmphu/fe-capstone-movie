import moment from 'moment/moment'
import React from 'react'

export default function MovieTabItem({movie}) {
    return (
      <div className='flex p-3'>
          <img className='object-cover mr-5 rounded' style={{width: '120px', height: '150px'}} src={movie.hinhAnh} alt='' />
          <div>
              <h5 className='font-semibold uppercase mb-2'>
              <span className='font-bold text-white border rounded bg-orange-500 mr-2 px-1'>C18</span>
              {movie.tenPhim}</h5>
              <div className='font-medium mb-5 grid grid-cols-4 gap-4'>
                  {movie.lstLichChieuTheoPhim.slice(0,8).map((item) => {
                      return <span className='border bg-indigo-50 rounded p-2'>{moment(item.ngayChieuGioChieu).format('MMMM Do YYYY, hh:mm a')}</span>
                  })}
              </div>
          </div>
      </div>
    )
  }
