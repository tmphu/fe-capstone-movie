import React from 'react'
import { Carousel } from 'antd';

export default function HomeCarouselDesktop({bannerArr}) {
  
  const handleRenderCarousel = () => {
    const contentStyle = {
      height: '90vh',
      color: '#fff',
      lineHeight: '160px',
      textAlign: 'center',
      backgroundPosition: 'center',
      backgroundSize: '100%',
      backgroundRepeat: 'no-repeat',
    };
    return bannerArr.map((item, index) => {
      return (
        
          <div key={index}>
            <div style={{...contentStyle, backgroundImage:`url(${item.hinhAnh})`}}>
              <img src={item.hinhAnh} className='w-full h-full opacity-1' alt="movie banner" />
            </div>
          </div>
        
      )
    })
  }

  return (
    <div>
      <Carousel className='w-full mx-0' autoplay dots={true} draggable effect='fade' >
        {handleRenderCarousel()}
      </Carousel>
    </div>
  )
}
