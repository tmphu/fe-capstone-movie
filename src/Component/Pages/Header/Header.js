/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import { NavLink } from 'react-router-dom'
import UserNav from './UserNav'

export default function Header({children}) {
  return (
    <div>
        <div className='fixed z-10 w-full flex px-10 py-5 shadow-lg shadow-indigo-300/30 justify-between items-center bg-indigo-50/80' style={{height: '80px'}}>
          <NavLink to='/'>
              <span className='text-gray-900 font-medium text-3xl drop-shadow-md antialiased transition duration-500 hover:text-indigo-700 hover:drop-shadow-2xl'>
                  CYBER.CINEMAX
              </span>
          </NavLink>
          <nav>
            <UserNav>{children}</UserNav>
          </nav>
        </div>
    </div>
  )
}
