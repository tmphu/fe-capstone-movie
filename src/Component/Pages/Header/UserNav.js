import React from "react";
import { useSelector } from "react-redux";
// import { NavLink } from 'react-router-dom';
import { userLocalService } from "../../../services/localStorageService";

export default function UserNav() {
  let user = useSelector((state) => {
    return state.userReducer.userInfo;
  });
  let handleLogout = () => {
    userLocalService.removeItem();
    window.location.href = "/";
  };
  const renderContent = () => {
    // nếu có user
    if (user) {
      return (
        <>
          Xin chào,
          <span className="text-indigo-700 font-bold text-xl">
            {" "}
            {user.hoTen}
          </span>
          <button
            onClick={handleLogout}
            className="border-1 rounded px-5 py-2 hover:drop-shadow-md hover:text-indigo-700 transition duration-500"
          >
            Đăng xuất
          </button>
        </>
      );
    } else {
      return (
        /*
            <>
              <NavLink to='/login'>
                <button className='border-1 rounded px-5 py-2 hover:drop-shadow-md hover:text-indigo-700 transition duration-500'>Đăng nhập</button>
              </NavLink>
              <NavLink to='/signup'>
                <button className='border-1 rounded px-5 py-2 hover:drop-shadow-md hover:text-indigo-700 transition duration-500'>Đăng ký</button>
              </NavLink>
            </>
            */

        <div>
          <div className="mt-3">
            <button
              title="Toogle User Menu"
              data-collapse-toggle="navbar-default"
              type="button"
              className="inline-flex items-center p-2 text-sm text-gray-500 rounded-lg md:hidden transition duration-300 hover:bg-indigo-100 focus:outline-none dark:text-indigo-400 dark:hover:bg-indigo-700 dark:focus:ring-gray-600"
              aria-controls="navbar-default"
              aria-expanded="false"
            >
              <svg
                class="w-6 h-6"
                aria-hidden="true"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill-rule="evenodd"
                  d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                  clip-rule="evenodd"
                ></path>
              </svg>
            </button>
            <div
              className="hidden w-full md:block md:w-auto"
              id="navbar-default"
            >
              <div className="sm:bg-indigo-50/50 md:bg-inherit sm:rounded md:rounded-none sm:border md:border-none sm:border-indigo-100 py-2">
                <ul class="flex flex-col md:flex-row md:space-x-8 md:mt-0 md:border-0">
                  <li>
                    <a
                      href="/login"
                      className="border-1 rounded px-2 py-2 hover:drop-shadow-md hover:text-indigo-700 transition duration-500"
                    >
                      Đăng nhập
                    </a>
                  </li>
                  <li>
                    <a
                      href="/signup"
                      className="border-1 rounded px-2 py-2 hover:drop-shadow-md hover:text-indigo-700 transition duration-500"
                    >
                      Đăng ký
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      );
    }
  };
  return <div className="space-x-5">{renderContent()}</div>;
}
