import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import moment from "moment";
import { movieService } from "../../../../services/movieService";

const onChange = (key) => {
  console.log(key);
};

export default function CinemaTabDesktop({ id }) {
  //   const listCinemas = movieData.heThongRapChieu;
  //   console.log("listCinemas", listCinemas);

  const [listCinemas, setListCinemas] = useState([]);
  useEffect(() => {
    movieService
      .getMovieSchedule(id)
      .then((res) => {
        setListCinemas(res.data.content.heThongRapChieu);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  console.log("listCinemas", listCinemas);

  let renderCinemas = () => {
    return listCinemas.map((cinema) => {
      return {
        label: (
          <div className="flex items-center">
            <img
              className="w-12 h-12 object-cover mr-3"
              src={cinema.logo}
              alt=""
            />
            <p>{cinema.maHeThongRap}</p>
          </div>
        ),
        key: cinema.maHeThongRap,
        children: renderBranches(cinema.cumRapChieu),
      };
    });
  };
  let renderBranches = (listBranches) => {
    return listBranches.map((branch) => {
      return (
        <div key={branch.maCumRap}>
          <div className="flex">
            <img
              src={branch.hinhAnh}
              alt=""
              style={{ width: "50px", marginRight: "10px" }}
            />
            <div>
              <p>{branch.tenCumRap}</p>
              <p>{branch.diaChi}</p>
            </div>
          </div>
          <div className="font-medium mb-5 grid grid-cols-8 gap-4">{renderSchedule(branch.lichChieuPhim)}</div>
        </div>
      );
    });
  };
  let renderSchedule = (schedules) => {
    return schedules.map((schedule) => {
      return (
        <span className="border bg-indigo-50 rounded p-2">
          {moment(schedule.ngayChieuGioChieu).format("DD-MM-YYYY, HH:MM ")}
        </span>
      );
    });
  };

  return (
    <div className="my-5">
      <Tabs
        defaultActiveKey="1"
        onChange={onChange}
        tabPosition="left"
        tabBarStyle={{ width: "200px" }}
        items={renderCinemas()}
      />
    </div>
  );
}
