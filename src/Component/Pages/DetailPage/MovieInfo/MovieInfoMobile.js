import React from "react";
import moment from "moment";
import bgPhoto from "../../../../assets/purple-stars.jpg";

export default function MovieInfoMobile({ movieData }) {
  const { hinhAnh, moTa, ngayKhoiChieu, danhGia, tenPhim, trailer } = movieData;

  const renderBreadcrumbs = () => {
    return (
      <div className="mb-5">
        <p>Trang chủ &#62; {tenPhim} </p>
      </div>
    );
  };

  const renderMovieInfo = () => {
    return (
      <div>
        <img src={hinhAnh} alt="" style={{ width: "100%" }} />
        <div className="flex flex-col justify-between">
          <div>
            <h2 className="font-medium">{tenPhim}</h2>
            <p>{moTa}</p>
            <p>Ngày chiếu: {moment({ ngayKhoiChieu }).format("DD/MM/YYYY")}</p>
            <p>Đánh giá: {danhGia}</p>
          </div>
          <div>
            <a
              href={trailer}
              className="bg-green-500 hover:bg-green-700 text-white py-2 px-4 rounded mr-5"
            >
              Xem trailer
            </a>
            <button className="bg-green-500 hover:bg-green-700 text-white py-2 px-4 rounded">
              Mua vé ngay
            </button>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className="p-5">
      {renderBreadcrumbs()}
      {renderMovieInfo()}
    </div>
  );
}
