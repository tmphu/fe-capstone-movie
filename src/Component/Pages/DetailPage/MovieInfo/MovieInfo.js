import React from 'react'
import { Desktop, Tablet, Mobile } from '../../../../HOC/Responsive';
import MovieInfoDesktop from './MovieInfoDesktop';
import MovieInfoTablet from './MovieInfoTablet';
import MovieInfoMobile from './MovieInfoMobile';

export default function MovieInfo({movieData}) {
  return (
    <div>
        <Desktop><MovieInfoDesktop movieData={movieData}/></Desktop>
        <Tablet><MovieInfoTablet movieData={movieData}/></Tablet>
        <Mobile><MovieInfoMobile movieData={movieData}/></Mobile>
    </div>
  )
}
