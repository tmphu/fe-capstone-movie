import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { movieService } from "../../../services/movieService";
import Footer from "../Footer/Footer";
import CinemaTab from "./CinemaTab/CinemaTab";
import MovieInfo from "./MovieInfo/MovieInfo";

/**
 * MovieInfo - component chi tiet phim
 * (breadcrumbs, movie photo, movie info,
 * trailer btn, checkout btn)
 * CinemaTab
 * Footer
 */

export default function DetailPage() {
  const { id } = useParams();
  const [movieData, setMovieData] = useState({});
  useEffect(() => {
    movieService
      .getMovieSchedule(id)
      .then((res) => {
        setMovieData(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  console.log('movieData2', movieData);
  console.log('movieData.heThongRapChieu', movieData.heThongRapChieu);
  return (
    <div>
      <MovieInfo movieData={movieData} />
      {/* <CinemaTab movieData={movieData} /> */}
      <CinemaTab id={id}/>
      <Footer />
    </div>
  );
}
