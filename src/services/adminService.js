import { https } from "./configURL";

export const adminService = {
    getUserList:() => {
        return https.get('/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP00')
        // add ?MaNhom=GP00 so we can see the new register account info
    },
    deleteUser:(idUser) => {
        return https.delete(`/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${idUser}`)
    }
};