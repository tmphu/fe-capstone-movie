import axios from "axios"
import { BASE_URL, createConfig } from "./configURL"
// import { https } from "./configURL"


export const userService = {
    // postLogin: (dataUser) => {
    //     return https.post(`api/QuanLyNguoiDung/DangNhap?ndDN=${dataUser}`)
    // },
    postLogin: (dataUser) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
            method: 'POST',
            data: dataUser,
            headers: createConfig(),
        })
    },
    postSignup: (dataUser) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyNguoiDung/DangKy`,
            method: 'POST',
            data: dataUser,
            headers: createConfig(),
        })
    },
    
}